import React , {useState ,} from 'react'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css'

export const CreateHero = () => {
    const [heroName , setNewHero] = useState(
        {name: ''}
    );
    const handleChange = (event) => {
        setNewHero(event.target.value)
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        axios.post('http://localhost:3012/hero' , {name: heroName})
            .then(function () {
                document.location.reload();

            })
        window.location.reload()
            .catch(function (error) {
                console.log(error)
            })
    }

    return (
        <div className="container">
            <form className='white' onSubmit={handleSubmit}>
                <h5 className="grey-text.text-darken-3">Create a new Hero</h5>
                <div className="input-field d-inline-flex">
                    <label htmlFor="lastName">Hero Name</label>
                    <input type="name" name="lastName" value={heroName.name} onChange={handleChange} required={true}/>
                    <button className="btn btn-success darken-3" type="submit">Add</button>
                </div>

            </form>
        </div>
    );
}
