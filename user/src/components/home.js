import React from 'react'
import {HeroList} from './heroList';
import {CreateHero} from "./createHero"
export const Home = () =>{
    return(
        <div>
            <h1> Home</h1>
            <CreateHero/>
            <HeroList/>

        </div>
    )
};