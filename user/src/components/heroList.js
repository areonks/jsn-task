import React , {Component} from 'react'
import {Link} from 'react-router-dom'
import {Button} from "reactstrap";
import {ListGroup , ListGroupItem} from "reactstrap";

import 'bootstrap/dist/css/bootstrap.min.css'
import axios from "axios";


export class HeroList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null ,
            isLoaded: false ,
            items: []
        }
    }

    componentDidMount() {
        fetch("http://localhost:3012/hero")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true ,
                        items: result
                    });
                } ,
                (error) => {
                    this.setState({
                        isLoaded: true ,
                        error
                    })
                }
            )
    }

    deleteHero = (id) => {
        axios.delete("http://localhost:3012/hero" , {
            data: {id}
        })
        window.location.reload()
    }

    render() {
        const {erorr , isLoaded , items} = this.state;
        if (erorr) {
            return <h1>fuuuu{erorr.message}</h1>
        } else if (!isLoaded) {
            return <h1>Load</h1>
        } else {
            return (
                <ListGroup className={'mx-auto list-wrapper'}>
                    {items.map(item => (
                        <ListGroupItem key={item.id} className={'align-items-xl-center'}>
                            <div className={'d-inline-block'}>{item.name}</div>
                            <img width={50} height={50} src={item.photo} alt={'error'}/>
                            <Link to={`/hero/${item.id}`} className='btn btn-success ml-2'> edit</Link>
                            <Button color='danger' onClick={this.deleteHero.bind(this , item.id)}>Delete</Button>
                        </ListGroupItem>

                    ))}
                </ListGroup>
            )
        }
    }
}
