import React from 'react'
import './App.css';
import {BrowserRouter as Router , Route , Switch} from "react-router-dom";
import {Home} from "./components/home";
import {EditHero} from "./components/editHero";

function App() {
    return (
        <div className="App">
                <Router>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route path='/hero/:id' component={EditHero}/>
                    </Switch>
                </Router>
        </div>
    );
}

export default App;
